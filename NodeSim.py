import telnetlib 
import random
import time
from sys import argv

#default node id
nodeId = 3

#absolute low and high slevels
absolutelow = 0
absolutehigh = 1

#time delay
delay = 15

#deviation
deviation = .1

#update based on passed arguments
nodeId = int(argv[1]) if len(argv)>1 else nodeId
delay = int(float(argv[2])) if len(argv)>2 else delay
absolutelow = float(argv[3]) if len(argv)>3 else absolutelow
absolutehigh = float(argv[4]) if len(argv)>4 else absolutehigh
deviation = float(argv[5]) if len(argv)>5 else deviation
steps = int(float(argv[6])) if len(argv)>6 else -1


#server info
Host = "50.142.48.24"
port = 5555
tn = telnetlib.Telnet(Host,port)

ID = str(nodeId)

random.seed()

val = random.triangular(absolutelow,absolutehigh);

x=0
while x < steps or steps==-1:#infinite loop unless steps set to positive value
	low = max(absolutelow,val-deviation)
	high = min(absolutehigh,val+deviation)
	val = random.triangular(low,high)
	SL =str(round(val,2))
	mystr = "SS " + ID + " " + SL + "\n"
	tn.write(mystr.encode('ascii'))
	time.sleep(delay)
	if(steps!=-1): 
		x += 1	
	
