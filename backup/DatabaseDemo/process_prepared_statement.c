/* #@ _INSERT_RECORDS_ */
static void
insert_rows (MYSQL_STMT *stmt)
{
char          *stmt_str = "INSERT INTO Measurements (nodeID,recordtime,slevel) VALUES(?,NOW(),?)";
MYSQL_BIND    param[2];
int           my_int;
float         my_float;
int           i;


  if (mysql_stmt_prepare (stmt, stmt_str, strlen (stmt_str)) != 0)
  {
    print_stmt_error (stmt, "Could not prepare INSERT statement");
    return;
  }

  /*
   * zero the parameter structures, then perform all parameter
   * initialization that is constant and does not change for each row
   */

  memset ((void *) param, 0, sizeof (param));

  /* set up INT parameter */

  param[0].buffer_type = MYSQL_TYPE_LONG;
  param[0].buffer = (void *) &my_int;
  param[0].is_unsigned = 0;
  param[0].is_null = 0;
  /* buffer_length, length need not be set */

  /* set up FLOAT parameter */

  param[1].buffer_type = MYSQL_TYPE_FLOAT;
  param[1].buffer = (void *) &my_float;
  param[1].is_null = 0;
  /* is_unsigned, buffer_length, length need not be set */

  if (mysql_stmt_bind_param (stmt, param) != 0)
  {
    print_stmt_error (stmt, "Could not bind parameters for INSERT");
    return;
  }

  while(!feof(stdin))
  {
    /* set the variables that are associated with each parameter */

    /* param[0]: set my_int value */
    printf("Enter node ID: ");
    scanf("%d",&my_int);

    if(feof(stdin)) break;

    /* param[1]: set my_float value */
    printf("Enter Sound Level: ");
    scanf("%f",&my_float);

    if (mysql_stmt_execute (stmt) != 0)
    {
      print_stmt_error (stmt, "Could not execute statement");
      return;
    }

  }
}
/* #@ _INSERT_RECORDS_ */


/* #@ _SELECT_RECORDS_ */
static void
select_rows (MYSQL_STMT *stmt)
{
char          *stmt_str = "SELECT nodeId, recordtime, slevel FROM Measurements";
MYSQL_BIND    param[3];
int           my_int;
float         my_float;
MYSQL_TIME    my_datetime;
my_bool       is_null[3];

  printf ("Retrieving records...\n");

  if (mysql_stmt_prepare (stmt, stmt_str, strlen (stmt_str)) != 0)
  {
    print_stmt_error (stmt, "Could not prepare SELECT statement");
    return;
  }

  if (mysql_stmt_field_count (stmt) != 3)
  {
    print_stmt_error (stmt, "Unexpected column count from SELECT");
    return;
  }

  /*
   * initialize the result column structures
   */

  memset ((void *) param, 0, sizeof (param)); /* zero the structures */

  /* set up INT parameter */

  param[0].buffer_type = MYSQL_TYPE_LONG;
  param[0].buffer = (void *) &my_int;
  param[0].is_unsigned = 0;
  param[0].is_null = &is_null[0];
  /* buffer_length, length need not be set */

    /* set up DATETIME parameter */

  param[1].buffer_type = MYSQL_TYPE_DATETIME;
  param[1].buffer = (void *) &my_datetime;
  param[1].is_null = &is_null[1];
  
  //* set up FLOAT parameter */

  param[2].buffer_type = MYSQL_TYPE_FLOAT;
  param[2].buffer = (void *) &my_float;
  param[2].is_null = &is_null[2];
  /* is_unsigned, buffer_length, length need not be set */

  /* is_unsigned, buffer_length, length need not be set */

  if (mysql_stmt_bind_result (stmt, param) != 0)
  {
    print_stmt_error (stmt, "Could not bind parameters for SELECT");
    return;
  }

  if (mysql_stmt_execute (stmt) != 0)
  {
    print_stmt_error (stmt, "Could not execute SELECT");
    return;
  }

  /*
   *(unsigned long)param[1].erroi fetch result set into client memory; this is optional, but it
   * enables mysql_stmt_num_rows() to be called to determine the
   * number of rows in the result set.
   */

  if (mysql_stmt_store_result (stmt) != 0)
  {
    print_stmt_error (stmt, "Could not buffer result set");
    return;
  }
  else
  {
    /* mysql_stmt_store_result() makes row count available */
    printf ("Number of rows retrieved: %lu\n",
            (unsigned long) mysql_stmt_num_rows (stmt));
  }
  while (mysql_stmt_fetch (stmt) == 0)  /* fetch each row */
  {
    /* display row values */
    printf ("%d  ", my_int);
    printf ("%04d-%02d-%02d %02d:%02d:%02d ",
            my_datetime.year,
            my_datetime.month,
            my_datetime.day,
            my_datetime.hour,
            my_datetime.minute,
            my_datetime.second);
    printf ("%.2f\n", my_float);
  }
  mysql_stmt_free_result (stmt);      /* deallocate result set */
}
/* #@ _SELECT_RECORDS_ */

/* #@ _PROCESS_PREPARED_STATEMENTS_ */
void
process_prepared_statements (MYSQL *conn)
{
MYSQL_STMT *stmt;
char       *use_stmt = "USE Sound";

  /* select database and create test table */

  if (mysql_query (conn, use_stmt) != 0)
  {
    print_error (conn, "Could access database Sound");
    return;
  }  

stmt = mysql_stmt_init (conn);  /* allocate statement handler */
  if (stmt == NULL)
  {
    print_error (conn, "Could not initialize statement handler");
    return;
  }

  /* insert and retrieve some records */
  insert_rows (stmt);
  select_rows (stmt);
  mysql_stmt_close (stmt);       /* deallocate statement handler */
}
/* #@ _PROCESS_PREPARED_STATEMENTS_ */

