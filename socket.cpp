#include <my_global.h>
#include <my_sys.h>
#include <m_string.h>   /* for strdup() */
#include <sys/select.h>
#include <ctime>
#include <list>
#include <queue>
#include <poll.h>
#include <string>
#include <mysql.h>
#include <my_getopt.h>
#include <pthread.h>
#define NODEPORT 5555
#define APPPORT 5556

extern "C"{ 
#include "sockettome.h"
}

const char * MostRecentQry = "SELECT * FROM Measurements WHERE recordtime between DATE_SUB(NOW(),INTERVAL 30 MINUTE) \
                              AND NOW() ORDER BY nodeId, recordtime ASC";

/**************************
   Database Parameters
**************************/

static char *opt_host_name = NULL;    /* server host (default=localhost) */
static char const *opt_user_name = "root";    /* username (default=login name) */
static char const *opt_password = "root";     /* password (default=none) */
static unsigned int opt_port_num = 0; /* port number (use built-in value) */
static char *opt_socket_name = NULL;  /* socket name (use built-in value) */
static char const *opt_db_name = "Sound";      /* database name (default=none) */
static unsigned int opt_flags = 0;    /* connection flags (none) */


//Struct to Hold Values From Connect App Thread to Subscribe App Thread
struct SubscribeData{
   int fd;                 //file stream to write to
   pthread_mutex_t fLock;  //mutex for atomic file writing
   int Term;               //1 -> terminate thread; 0 - > do nothing
};


//Struct to Hold Queue Information for Each Subscription
struct SafeQueue{
   std::queue< std::string>* queueptr; //holds subscribe strings to send to Apps
   pthread_mutex_t qLock;              //mutex to guard queue
};


//Global Variables 
std::list<struct SafeQueue*> QueueList;   //List of All Subscription Queues 
pthread_mutex_t ListLock;                 //Guards the List
pthread_mutex_t CondLock;                 //Mutex For Subscribe App Sleeping
pthread_cond_t  NodeUpdated;              //Condition Variable to wake up Sub Threads


/*************************************
   Add Queue to Global List of Queues
*************************************/
void AddQueue(struct SafeQueue* q){
   pthread_mutex_lock(&ListLock);
   QueueList.push_back(q);
   pthread_mutex_unlock(&ListLock);
}

/*****************************************
   Remove Queue from Global List of Queues
*****************************************/
void RemoveQueue(struct SafeQueue* q){
   pthread_mutex_lock(&ListLock);
   QueueList.remove(q);
   pthread_mutex_unlock(&ListLock);
}

/*****************************************
   Queue Measurement 
*****************************************/
void QueueMeasurement( std::string str){
   //Add String to All Queues in List
   pthread_mutex_lock(&ListLock);
   for (std::list<struct SafeQueue*>::iterator it=QueueList.begin(); it != QueueList.end(); it++){
      struct SafeQueue* QElem = *it;
      pthread_mutex_lock(&(QElem->qLock));
      QElem->queueptr->push(str);
      pthread_mutex_unlock(&(QElem->qLock));
   }
   pthread_mutex_unlock(&ListLock);

   //Wake Up All Threads Waiting on Condition NodeUpdated
   printf("Node About to Call Broadcast\n");
   pthread_cond_broadcast(&NodeUpdated);
}


/****************************************
   File Print - print cstring to a file descriptor
****************************************/

void FilePrint(int fd, const char * str){      
      int i=1,indx=0;
      while(i>0 && str[indx]!='\0') {
         i=write(fd,&str[indx++],1);
      }
   }

/**************************
   Safe Fprintf - locks mutex befor printing string
**************************/
   void SafeFilePrint(int fd, const char * str, pthread_mutex_t * mtx){
      //printf("Aquire File Mutex to print %s on fd %i\n",str,fd);
      pthread_mutex_lock(mtx);
       
      FilePrint(fd, str);                   
      
      pthread_mutex_unlock(mtx);
      //printf("Released File Mutex\n");
   }


/****************************************
   Safefgets - fgets which uses lock
****************************************/
// char * Safefgets(char * stmt, int len,FILE* fs,pthread_mutex_t * mtx){
//   char * retval;
//   pthread_mutex_lock(mtx);
//   retval = fgets(stmt,len,fs);
//   pthread_mutex_unlock(mtx);
//   return retval;
//}

/****************************************
   MyFgets: Fgets implemented with fd's and read()
****************************************/
void Myfgets(char * str, int len, int fd){
   char c;           
   int i,indx=0;               
                       
   i = read(fd, &c, 1);   
   while(i > 0 && c!='\n' && c!='\0' && indx <len) {
      str[indx++]=c;   
      i = read(fd, &c, 1); 
   }
   if(c=='\n'){
      str[indx++]='\n';
   }
   str[indx]='\0';
}

/**************************
   Print MYSQL Errors
**************************/

static void
print_error (MYSQL *conn, char const *message){
   fprintf (stderr, "%s\n", message);
   if (conn != NULL){
      fprintf (stderr, "Error %u (%s): %s\n",
      mysql_errno (conn), mysql_sqlstate (conn), mysql_error (conn));
   }  
}

/**************************
   Print MYSQL Stmt Errors
**************************/

static void
print_stmt_error (MYSQL_STMT *stmt, char const *message){
   fprintf (stderr, "%s\n", message);
   if (stmt != NULL){
      fprintf (stderr, "Error %u (%s): %s\n",
         mysql_stmt_errno (stmt),
         mysql_stmt_sqlstate (stmt),
         mysql_stmt_error (stmt));
   }
}

/**************************
   Process a Result Set
**************************/
void process_result_set(MYSQL *conn, MYSQL_RES *res_set,int  fd,pthread_mutex_t * FileLock){
   MYSQL_ROW row;
   unsigned int i;
   char str[200];
   SafeFilePrint(fd,"R\n",FileLock);
   //printf("Aquire Mutex for returning query result\n");
   pthread_mutex_lock(FileLock);
   while((row=mysql_fetch_row(res_set))!=NULL){
      for(i=0; i< mysql_num_fields(res_set);i++){
         if(i>0){
            FilePrint(fd,",");
         }
         snprintf(str,200,"%s",row[i] != NULL ? row[i] : "NULL");
         FilePrint(fd,str);
      }
      FilePrint(fd,"\n");
   }
   pthread_mutex_unlock(FileLock);
   //printf("Released File Mutex\n");

   if(mysql_errno(conn) != 0){ 
      print_error(conn,"mysql_fetch_row() failed");
      SafeFilePrint(fd,"E\n",FileLock);
   }
   else SafeFilePrint(fd,"F\n",FileLock);
}

/**************************
   Process a MYSQL String
**************************/

void process_statement (MYSQL *conn, const char *stmt_str,int fd,pthread_mutex_t * FileLock){
   MYSQL_RES *res_set;

   //EXECUTE STATEMENT
   if (mysql_query (conn, stmt_str) != 0){  
      print_error (conn, "Could not execute statement for app");
      SafeFilePrint(fd,"E\n",FileLock);
      return;
   }

   //SEE IF DATA RETURNED
   res_set = mysql_store_result (conn);
   if (res_set){      
      process_result_set (conn, res_set,fd,FileLock);
      mysql_free_result (res_set);
   }

   //SEE WHY THERE IS NO DATA
   else{
    //SEE IF DATA SHOULD HAVE BEEN RETURNED
      if (mysql_field_count (conn) == 0){
         SafeFilePrint(fd,"F\n",FileLock);
      }
      else{
         print_error (conn, "Could not retrieve result set");
         SafeFilePrint(fd,"E\n",FileLock);
      }
   }
}

/*************************************
   Read From Socket and Insert Records
*************************************/

void ReadAndInsert(MYSQL_STMT *stmt, int *fd){
   //VARIABLES FOR FUNCTION
   char const    *stmt_str = "INSERT INTO Measurements (nodeID,recordtime,slevel) VALUES(?,?,?)";
   MYSQL_BIND    param[3];
   int           rv;

   //VARIABLES INTO WHICH TO READ DATA FROM CLIENTS
   char scode[50];
   int newNodeID;
   MYSQL_TIME my_datetime;
   float newSLevel;
   struct tm *cur_time;
   time_t clk;
   char queuestr[45], timestr[20];
    std::string qstr;

   //PREPARE STATEMENT
   if (mysql_stmt_prepare (stmt, stmt_str, strlen (stmt_str)) != 0){
      print_stmt_error (stmt, "Could not prepare INSERT statement");
      return;
   }

   //SETUP PARAMETER STRUCTURES
   memset ((void *) param, 0, sizeof (param)); /*zero out the structures*/

   /* set up NODEID parameter */
   param[0].buffer_type = MYSQL_TYPE_LONG;
   param[0].buffer = (void *) &newNodeID;
   param[0].is_unsigned = 0;
   param[0].is_null = 0;

   /* set up DATETIME parameter*/
   param[1].buffer_type = MYSQL_TYPE_DATETIME;
   param[1].buffer = (void *) &my_datetime;
   param[1].is_null = 0;

   /* set up SLEVEL parameter */
   param[2].buffer_type = MYSQL_TYPE_FLOAT;
   param[2].buffer = (void *) &newSLevel;
   param[2].is_null = 0;

   //BIND STRUCTURES TO THE STATEMENT
   if (mysql_stmt_bind_param (stmt, param) != 0){
      print_stmt_error (stmt, "Could not bind parameters for INSERT");
      return;
   }

   //READ ALL DATAPOINTS FROM NODE AND ADD THEM
   FILE *fsock = fdopen(*fd, "r"); /*open communication through socket*/
   while((rv=fscanf(fsock,"%s",scode))!=EOF){
      if(rv!=1){ 
         fprintf(stderr,"Problem Reading line\n");
         continue;
      }
      else{
         if(strncmp(scode,"SS",50)!=0){
            continue;
         }
         else{
            if((rv = fscanf(fsock,"%i %f",&newNodeID,&newSLevel))==EOF){
               break;
            }
            if(rv!=2){
               fprintf(stderr,"Problem Reading line\n");
               continue;
            }
            printf("received value of %f from node %i\n", newSLevel,newNodeID);
            //get current time
            time(&clk);
            cur_time = localtime(&clk);
            my_datetime.year = cur_time->tm_year + 1900;
            my_datetime.month = cur_time->tm_mon + 1;
            my_datetime.day = cur_time->tm_mday;
            my_datetime.hour = cur_time->tm_hour;
            my_datetime.minute = cur_time->tm_min;
            my_datetime.second = cur_time->tm_sec;
            my_datetime.second_part = 0;
            my_datetime.neg = 0;

            //make string to add to queues
            strftime(timestr,20,"%F %T",cur_time);
            snprintf(queuestr,45,"SUB\n%i,%s,%.2f\n",newNodeID,timestr,newSLevel);
            qstr = queuestr;
            QueueMeasurement(qstr);

            if (mysql_stmt_execute (stmt) != 0){ 
               print_stmt_error (stmt, "Could not execute statement for node insert");
            }
         }
      }
   }
   fclose(fsock);
}

/********************************
   Setup MYSQL Prepared Statement
********************************/

void SetupAndInsert(MYSQL *conn,int *fd){
   MYSQL_STMT *stmt;
   char const *use_stmt = "USE Sound";

   //RUN COMMAND TO SELECT SOUND DATABASE
   if (mysql_query (conn, use_stmt) != 0){
      print_error (conn, "Could not access database Sound");
      return;
   }  

   //MAKE A STATEMENT HANDLER
   stmt = mysql_stmt_init (conn);  
   if (stmt == NULL){
      print_error (conn, "Could not initialize statement handler");
      return;
   }

   //USE HANDLER TO READ/INSERT RECTORDS
   ReadAndInsert(stmt,fd);

   mysql_stmt_close (stmt); /* deallocate statement handler */
}

/****************************************
   Thread: Connect to Database and Insert
*****************************************/

void *ThreadInsert(void *fd){
   //DETATCH THREAD
   pthread_detach(pthread_self());

   //SETUP CONNECTION HANDLER
   MYSQL *conn; 
  
   conn = mysql_init (NULL);  /* initialize connection handler */
   if (conn == NULL){
      print_error (NULL, "mysql_init() failed (probably out of memory)");
      pthread_exit (NULL);
   }

   /* connect to server */
   if (mysql_real_connect (conn, opt_host_name, opt_user_name, opt_password,
      opt_db_name, opt_port_num, opt_socket_name, opt_flags) == NULL)
   {
      print_error (conn, "mysql_real_connect() failed");
      mysql_close (conn);
      pthread_exit (NULL);
   }

   //PREPARE STATEMENT AND READ/INSERT RECORDS
   SetupAndInsert(conn,(int *)fd);
   fprintf(stderr,"Closing Connection to Node\n");
   //CLOSE CONNECTION HANDLER AND EXIT
   mysql_close (conn);
   pthread_exit(NULL); 
}

/****************************************
   Thread: App Subscribes to Database
****************************************/
void* SubscribeApp(void *SD){
   std::string dp;
   pthread_detach(pthread_self());
   int fd = ((struct SubscribeData *)SD)->fd;

   //pthread_mutex_t * QLock = (pthread_mutex_t*)malloc(sizeof(pthread_mutex_t));
   //pthread_mutex_init(QLock,NULL);

   struct SafeQueue * MySafeQueue = (struct SafeQueue *)malloc(sizeof(struct SafeQueue));
   MySafeQueue->queueptr = new std::queue< std::string>();
   pthread_mutex_init(&(MySafeQueue->qLock),NULL);
   AddQueue(MySafeQueue);

   char str[201];
   while(1){
      //Check if App Signaled to terminate
      if(((struct SubscribeData *)SD)->Term == 1){
         break;
      }

      //Pop All Values from Queue and Write them to App
      //printf("Subscribed App waiting for Q lock\n");
      pthread_mutex_lock(&(MySafeQueue->qLock));
      while(!MySafeQueue->queueptr->empty()){
         //printf("Subscribed App printing node value\n");
         dp=MySafeQueue->queueptr->front();
         snprintf(str,200,"%s",dp.c_str()); //SUB keyword already added to queued strings
         SafeFilePrint(fd, str, (&((struct SubscribeData *)SD)->fLock));
         MySafeQueue->queueptr->pop();
         //printf("Subscribed App finished printing node value\n");
      }
      //printf("Subscribed App emptied Queue\n");
      pthread_mutex_unlock(&(MySafeQueue->qLock));
     // printf("Subscribed App unlocked Queue Mutex\n");

      //Sleep until Signaled by Node
      pthread_mutex_lock(&CondLock);
      //printf("Subscribed App about to sleep\n");
      pthread_cond_wait(&NodeUpdated,&CondLock);
      pthread_mutex_unlock(&CondLock);
      //printf("Subscribed App woke up\n");
   }

   //Cleanup
   printf("Closing Subscribe Thread\n");
   close(fd);
   RemoveQueue(MySafeQueue);
   delete MySafeQueue->queueptr;
   pthread_mutex_destroy(&((struct SubscribeData *)SD)->fLock);
   free(MySafeQueue);
   free((struct SubscribeData *)SD);
   pthread_exit(NULL); 
}

/****************************************
   Thread: Connect App to Database
*****************************************/

void *ConnectApp(void *fd){
   //DETATCH THREAD
   pthread_detach(pthread_self());

   int already_subscribed =0;
   pthread_t thread;

   //Make Structure to Send Subscribe App
   // includes a mutex to lock file, file descriptor, and terminate signal
   struct SubscribeData* SubData = (struct SubscribeData *)malloc(sizeof(struct SubscribeData));
   pthread_mutex_init(&(SubData->fLock), NULL);
   SubData->fd = *((int *)fd);
   SubData->Term = 0;

   //SETUP CONNECTION HANDLER
   MYSQL *conn;

   conn = mysql_init (NULL);  /* initialize connection handler */
   if (conn == NULL){
      print_error (NULL, "mysql_init() failed (probably out of memory)");
      pthread_exit (NULL);
   }

   /* connect to server */
   if (mysql_real_connect (conn, opt_host_name, opt_user_name, opt_password,
      opt_db_name, opt_port_num, opt_socket_name, opt_flags) == NULL)
   {
      print_error (conn, "mysql_real_connect() failed");
      mysql_close (conn);
      pthread_exit (NULL);
   }

   struct pollfd pfds[1];

   char Statement[201];
   //int error = 0;

   //Read Statment from Socket
   while(1){
      pfds[0].fd = SubData->fd;
      pfds[0].events = POLLIN;
      poll(pfds, 1, -1);
      if (pfds[0].revents & POLLHUP) {
         break;
      }
      else if(pfds[0].revents & POLLIN){
         Myfgets(Statement,201,SubData->fd);
         if(strncmp("SUBSCRIBE",Statement,9)==0){
            if(already_subscribed==0){
               already_subscribed=1;
               process_statement(conn, MostRecentQry,SubData->fd,&(SubData->fLock)); //send most recent node values
               pthread_create(&thread,NULL,SubscribeApp,SubData);
            }
         }
         else{
            //printf("Process statement with fd:%i",SubData->fd);
            process_statement(conn,Statement,SubData->fd,&(SubData->fLock));
         }
      }
      //printf("About to Loop Back to Poll\n");
   }
   SubData->Term = 1;
   fprintf(stderr,"Closing connection to App\n");

   //If Subscribe App not Made, cleanup
   if(already_subscribed==0){
      close(SubData->fd);
      pthread_mutex_destroy(&(SubData->fLock));
      free(SubData);
   }

   //CLOSE CONNECTION HANDLER AND EXIT
   mysql_close (conn);
   pthread_exit(NULL); 
}

/**************************
   Thread:Node Socket Master
**************************/
void *ThreadNSM(void *socket){
   pthread_detach(pthread_self());
   int fd, *sock=(int *)socket;
   pthread_t thread;
   while(1){
      fd = accept_connection(*sock);
      printf("Node Thread Master: Made thread with fd %i\n",fd);
      pthread_create(&thread,NULL,ThreadInsert,&fd);
   }
}

/**************************
   Thread:App Socket Master
**************************/
void *ThreadASM(void *socket){
   pthread_detach(pthread_self());
   int fd, *sock=(int *)socket;
   pthread_t thread;
   while(1){
      fd = accept_connection(*sock);
      printf("App Thread Master: Made thread with fd %i\n",fd);
      pthread_create(&thread,NULL,ConnectApp,&fd);
   }
}

/**************************
   MAIN FUNCTION
**************************/

int main(int argc, char **argv){
   int Nsocket, Asocket,rv;
   pthread_t NSMthread, ASMthread;
   char command[100];

   //SETUP MUTEX'S
   pthread_mutex_init(&ListLock,NULL); //protects list of queues
   pthread_mutex_init(&CondLock,NULL); //used for cond_wait
   pthread_cond_init(&NodeUpdated,NULL); //
   
   
   //SETUP MYSQL LIBRARIES
   MY_INIT (argv[0]);

   /* initialize client library */
   if (mysql_library_init (0, NULL, NULL)){
      print_error (NULL, "mysql_library_init() failed");
      exit (1);
   }
  

   //OPEN SOCKET AND ACCEPT CONNECTIONS
   Nsocket = serve_socket(NODEPORT);
   pthread_create(&NSMthread,NULL,ThreadNSM,&Nsocket);

   //OPEN SOCKET AND ACCEPT CONNECTIONS
   Asocket = serve_socket(APPPORT); 
   pthread_create(&ASMthread,NULL,ThreadASM,&Asocket);


   //Listen and exit if exit is typed
   while((rv=fscanf(stdin,"%s",command))!=EOF){
      if(strncmp(command,"exit",100)==0){
         break;
      }
   }
   
   pthread_mutex_destroy(&ListLock);
   pthread_mutex_destroy(&CondLock);
   pthread_cond_destroy(&NodeUpdated);

   //DISCONNECT FROM SERVER AND EXIT
   mysql_library_end ();
   exit(0);
}