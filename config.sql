CREATE DATABASE IF NOT EXISTS Sound;
USE Sound;
DROP TABLE IF EXISTS Measurements;
CREATE TABLE Measurements(
	nodeId INT NOT NULL,
	recordtime DATETIME NOT NULL,
	slevel FLOAT,
	PRIMARY KEY (nodeId, recordtime)
);
