# C and C++ compilers 
CC = gcc -g -Wall
CXX = g++ -g -Wall

# Use these settings if you have mysql_config.  Note that if you have
# multiple installations of MySQL, you can compile with the client
# library from any of them by changing the value of MYSQL_CONFIG to
# the full pathname of the version of mysql_config that you want to
# use.
MYSQL_CONFIG = mysql_config
INCLUDES = ${shell $(MYSQL_CONFIG) --include}
LIBS = ${shell $(MYSQL_CONFIG) --libs}
EMBLIBS = ${shell $(MYSQL_CONFIG) --libmysqld-libs}

# Use these settings if you don't have mysql_config; modify as necessary
#INCLUDES = -I/usr/local/mysql/include/mysql
#LIBS = -L/usr/local/mysql/lib/mysql -lmysqlclient #-lm -lsocket -lnsl
#EMBLIBS = -L/usr/local/mysql/lib/mysql -lmysqld #-lm -lsocket -lnsl

ALL_PROGRAMS =socket

default::
	@echo "You must say what you want to build.  For example:"
	@echo "make all - build all programs"
	@echo "make progname - build the 'progname' program"
	@echo "Permitted values of progname include:"
	@echo "$(ALL_PROGRAMS)"

all:: $(ALL_PROGRAMS)

.cpp.o:
	$(CXX) -c $(INCLUDES) $<

.c.o:
	$(CC) -c $(INCLUDES) $<
# Connect with socket and display values
socket:: socket.o sockettome.o
	$(CXX) -o $@ socket.o sockettome.o $(LIBS)

clean::
	rm -f $(ALL_PROGRAMS) *.o

